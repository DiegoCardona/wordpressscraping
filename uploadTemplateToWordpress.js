
var mysql = require('mysql');
const puppeteer = require('puppeteer');
require('dotenv').config()
const cp = require('child_process');


setElementStatus = function(id, status){
	 var con = mysql.createConnection({
	  host: process.env.DB_HOST,
	  user: process.env.DB_USER,
	  password: process.env.DB_PASS,
	  database: process.env.DB_NAME
	});

	 var sql = "UPDATE element SET status = "+mysql.escape(status)+" WHERE id = "+mysql.escape(id);
	  con.query(sql, function (err, result) {
	    if (err) throw err;
	    console.log(result.affectedRows + " record(s) updated");
	    con.destroy();
	  });
}

setElementType = function(id, type){
	 var con = mysql.createConnection({
	  host: process.env.DB_HOST,
	  user: process.env.DB_USER,
	  password: process.env.DB_PASS,
	  database: process.env.DB_NAME
	});

	 var sql = "UPDATE element SET type = "+mysql.escape(type)+" WHERE id = "+mysql.escape(id);
	  con.query(sql, function (err, result) {
	    if (err) throw err;
	    console.log(result.affectedRows + " record(s) updated");
	    con.destroy();
	  });
}


getElements = function(){
  return new Promise(function(resolve, reject){
  	var con = mysql.createConnection({
	  host: process.env.DB_HOST,
	  user: process.env.DB_USER,
	  password: process.env.DB_PASS,
	  database: process.env.DB_NAME
	});
    con.query(
        "SELECT* FROM element where status = 'DOWNLOADED' ", 
        function(err, rows){       
        	con.destroy();                                         
            if(rows === undefined){
                reject(new Error("Error rows is undefined"));
            }else{
                resolve(rows);
            }
        }
    )
    
	}
)};

(async () => {

  console.log('consulta de elementos')
  let elements = "";
  await getElements().then(function(result){
  	elements = result;
  });

  if(elements.length == 0){
  	return;
  }

  console.log('praparando para inicio de sesion')
  const browser = await puppeteer.launch({headless: true, args: ['--no-sandbox']});
  const page = await browser.newPage();
  await page.goto(process.env.WP_URL);
  await page.type('#user_login', process.env.WP_USER);
  await page.type('#user_pass', process.env.WP_PASS);
  await page.waitForSelector('#wp-submit');
  await page.click('#wp-submit');
  console.log('sesion iniciada')
  await page.waitForNavigation();
  

console.log(elements)

	if(elements.length > 0){
	  for (var i = 0; i < 1; i++) {    

	  	try{
	  		if(elements[i].type == "UPDATED"){
		  		search = elements[i].name.replace("-", ' ')
				search = search.replace(" – ", ' ')
				search = search.replace(" | ", ' ')
				search = search.replace(/-/g, ' ')
				console.log('buscando single '+ process.env.WP_URLPOST + search)
				await page.goto(process.env.WP_URLPOST + search);

				var names = await page.$$eval('.row-title', links => { return links.map(link => link.textContent).slice(0, 10) });
				var urls = await page.$$eval('.row-title', links => { return links.map(link => link.href).slice(0, 10) });

				console.log(names)
				console.log(urls)

				var product_url = "";
				for (var j = 0; j < urls.length; j++) {
					if(names[j] == elements[i].name){
						console.log('producto encontrado');
						console.log(names[j] +'=='+ elements[i].name)
						product_url = urls[j]
						console.log('url ' + product_url)
					}
				}

				if(product_url == ""){
					console.log('producto no encontrado')
					console.log(elements[i].name)
					console.log(names)
					setElementType(elements[i].id, 'NEW')
					continue;
				}

				await page.goto(product_url);


				
				//TODO: datos para borrar
				// const element = await page.$("#edd-edd_download_files1name-wrap > input");
				// var nombre_archivo = await page.evaluate(val => document.querySelector('#edd-edd_download_files1name-wrap > input').value);
				// var url_archivo = await page.evaluate(val => document.querySelector('#edd-edd_download_files1file-wrap > input').value);
				// console.log(nombre_archivo +" - "+ url_archivo)
				//edd_download_files[1][file]
				console.log('configurando acf')
				await page.evaluate(val => document.querySelector('#acf-field_5f444edb67159').value = val, elements[i].version);
				await page.evaluate(val => document.querySelector('#acf-field_5f444ef884946').value = val, elements[i].last_updated_date);
				await page.evaluate(val => document.querySelector('#acf-field_5f444f01b8758').value = val, 'GLP');
				


				var file = elements[i].file+''
				file = file.replace("/root/diego/WordpressScraping/downloads/", '')
				console.log('canbiando textos')
				await page.evaluate(val => document.querySelector('#edd-edd_download_files1name-wrap > input').value = val, file);
				await page.evaluate(val => document.querySelector('#edd-edd_download_files1file-wrap > input').value = val, process.env.WP_REMOTE_CONN_FILE+file);

				console.log('guardando')
				await page.click('#publish');

				setElementStatus(elements[i].id, 'UPLOADED')


			} 
			if(elements[i].type == "NEW"){
				//para nuevos
				await page.goto(process.env.WP_REMOTE_CONN_NEW);

				console.log('configurando acf')
				await page.evaluate(val => document.querySelector('#title').value = val, elements[i].name);
				await page.evaluate(val => document.querySelector('#acf-field_5f444edb67159').value = val, elements[i].version);
				await page.evaluate(val => document.querySelector('#acf-field_5f444ef884946').value = val, elements[i].last_updated_date);
				await page.evaluate(val => document.querySelector('#acf-field_5f444f01b8758').value = val, 'GLP');
				


				var file = elements[i].file+''
				file = file.replace("/root/diego/WordpressScraping/downloads/", '')
				console.log('canbiando textos')
				await page.evaluate(val => document.querySelector('#edd-edd_download_files1name-wrap > input').value = val, file);
				await page.evaluate(val => document.querySelector('#edd-edd_download_files1file-wrap > input').value = val, process.env.WP_REMOTE_CONN_FILE+file);

				console.log('guardando')
				await page.click('#save-post');

				setElementStatus(elements[i].id, 'UPLOADED')

			}
	  	}catch(error){
	  		console.log("no encontro producto")
			console.log(error);
			setElementStatus(elements[i].id, 'ERROR')
	  	}

	  }
	}



  // const data = await page.evaluate(() => {
  //   const tds = Array.from(document.querySelectorAll('#content > div > div > div > table:nth-child(2) > tbody'))
  //   return tds.map(td => td.innerText)
  // });
  // console.log('a guardar data')
  // processData(data)
  await page.screenshot({path: 'example.png'});
  await browser.close();
  console.log('fin');
})();

