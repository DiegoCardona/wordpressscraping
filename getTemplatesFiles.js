
var mysql = require('mysql');
const puppeteer = require('puppeteer');
require('dotenv').config()
const cp = require('child_process');
const request = require('request');
const fs = require('fs');
// var save_elements = function(){

// }


setElementStatus = function(id, status, file = ''){
	 var con = mysql.createConnection({
	  host: process.env.DB_HOST,
	  user: process.env.DB_USER,
	  password: process.env.DB_PASS,
	  database: process.env.DB_NAME
	});

	 var sql = "UPDATE element SET status = "+mysql.escape(status)+", file= "+mysql.escape(file)+" WHERE id = "+mysql.escape(id);
	  con.query(sql, function (err, result) {
	    if (err) throw err;
	    console.log(result.affectedRows + " record(s) updated");
	    con.destroy();
	  });

	  

}

getElements = function(){
  return new Promise(function(resolve, reject){
  	var con = mysql.createConnection({
	  host: process.env.DB_HOST,
	  user: process.env.DB_USER,
	  password: process.env.DB_PASS,
	  database: process.env.DB_NAME
	});
	//SELECT * FROM element where status = 'NEW' UNION SELECT * FROM element where status = 'ERROR' ORDER BY id DESC LIMIT 20 
    con.query(
        "SELECT * FROM (SELECT * FROM element where status = 'NEW' UNION SELECT * FROM element where status = 'ERROR' ORDER BY id DESC LIMIT 20) DATA ORDER BY status DESC ", 
        function(err, rows){       
        	con.destroy();                                         
            if(rows === undefined){
                reject(new Error("Error rows is undefined"));
            }else{
                resolve(rows);
            }
        }
    )
    
	}
)}

function processData(data){
	//console.log(JSON.stringify(data));
	data = data  + '';
	data = data.split("\n");
	for (var i = 0; i < data.length; i++) {
		data[i] = data[i].replace(/\t/g, '')
		data[i] = data[i].split(" ");
		data[i][data[i].length -1] = "";
		data[i] = data[i].join(' ').trim();
		if(data[i].indexOf("UPDATE") == 0){
			data[i] = data[i].replace('UPDATE','')
			saveElement(data[i], "UPDATED", "NEW")
		}
		if(data[i].startsWith("NEW")){
			data[i] = data[i].replace('NEW','')
			saveElement(data[i], "NEW", "NEW")
		}
	}
}

// let download = async function(uri, filename){
//     // let command = `curl -o ${filename}  '${uri}'`;
//     let command = `curl  '${uri}'`;
//     let result = cp.execSync(command);
// };

function formatWget(url, cookies){
	var headers = "cookie:"
	cookies = cookies['cookies'];
	for (var i = 0; i < cookies.length; i++) {
		if(cookies[i].name.indexOf("quform_session_") == 0){
			headers += cookies[i].name +'='+cookies[i].value + ';';
		}
		if(cookies[i].name.indexOf("_lscache_vary") == 0){
			headers += cookies[i].name +'='+cookies[i].value + ';';
		}
		if(cookies[i].name.indexOf("wordpress_logged_in_") == 0){
			headers += cookies[i].name +'='+cookies[i].value + ';';
		}
	}
	return headers;
}

function download(uri, headers, zip, callback) {
    return new Promise(function (resolve, reject) {
       	const execFile = require('child_process').execFile;

        var args = [
            '--output-document', zip,
            '--header', headers,
            uri
        ];
        console.log(args)
        const child = execFile('wget', args, (err, stdout, stderr) => {
            console.log(err)
            console.log(stdout.toString());
            console.log('archivo descargando interno en download')
            return resolve('ready');
        });

    });
}

(async () => {
	console.log('init')
  
  let elements = "";
  await getElements().then(function(result){
  	elements = result;
  })
  console.log('se consultaron los pendientes')
  if(elements.length == 0){
  	return;
  }

  const browser = await puppeteer.launch({headless: true, args: ['--no-sandbox']});
  const page = await browser.newPage();
  await page.goto(process.env.CHANGE_LOG_URL);
  await page.type('#username', process.env.CHANGE_LOG_USER);
  await page.type('#password', process.env.CHANGE_LOG_PASS);
  await page.waitForSelector('button[name="login"]');
  await page.click('button[name="login"]');
  await page.waitForNavigation();
  console.log('sesion iniciada')
  	for (var i = 0; i < 1; i++) {    
  // for (var i = 0; i < 10; i++) {  
  	console.log('****************************')
		try {
		  search = elements[i].name.replace("-", ' ')
		  search = search.replace(" – ", ' ')
		  search = search.replace(" | ", ' ')
		  search = search.replace(/-/g, ' ')
		  // search = search.replace(/|/g, ' ')
		  // search = search.replace(/ & /g, ' ')
		  // search = search.replace(/&/g, ' ')

		  console.log('buscando single '+ process.env.CHANGE_LOG_URL_SEARCH + search)
		  await page.goto(process.env.CHANGE_LOG_URL_SEARCH + search);
		  await page.waitForSelector(".single-product", { timeout: 2000 })
		  console.log('encontro producto')

			var url = await page.$$eval('.red-link', anchors => [].map.call(anchors, a => a.href));
			if(url.length <= 0){
				setElementStatus(elements[i].id, 'ERROR');
				continue;
			}
			
		  	var zip = '/root/diego/WordpressScraping/downloads/'+search.split(" ").join('-')+'.zip'

	            var cookies = await page._client.send('Network.getAllCookies');
	            var headers = formatWget(url[0], cookies);
	            console.log('headers ' + headers);
				await download(url[0], headers, zip , function() {
				   console.log('downloaded');
				});
	            console.log('despues de descargar')
			
		  	setElementStatus(elements[i].id, 'DOWNLOADED', zip)
		} catch (error) {
		  
		  try{
		  	  
			  search = elements[i].name.replace("-", ' ')
			  search = search.replace(" – ", ' ')
			  search = search.replace(" | ", ' ')
			  search = search.replace(/-/g, ' ')
			  // search = search.replace(/|/g, ' ')
			  // search = search.replace(/ & /g, ' ')
			  // search = search.replace(/&/g, ' ')

			  console.log('buscando multiple '+ process.env.CHANGE_LOG_URL_SEARCH + search)
			  await page.goto(process.env.CHANGE_LOG_URL_SEARCH + search);
			  console.log('verificando si tuvimos multiples respuestas	')

			  var names = await page.$$eval('.product-title > a', links => { return links.map(link => link.textContent).slice(0, 10) });
			  var urls = await page.$$eval('.product-title > a', links => { return links.map(link => link.href).slice(0, 10) });
			  var product_url = "";
			  for (var j = 0; j < urls.length; j++) {
			  	if(names[j] == elements[i].name){
			  		console.log('producto encontrado');
			  		console.log(names[j] +'=='+ elements[i].name)
			  		product_url = urls[j]
			  		console.log('url ' + product_url)
			  	}
			  }
			  
			 if(product_url == ""){
			 	console.log('producto no encontrado')
			 	console.log(elements[i].name)
			 	console.log(names)
			 	setElementStatus(elements[i].id, 'ERROR')
			 	continue;
			 }

			 await page.goto(product_url);
			  await page.waitForSelector(".single-product", { timeout: 2000 })
			  console.log('encontro producto')

				var url = await page.$$eval('.red-link', anchors => [].map.call(anchors, a => a.href));
				if(url.length <= 0){
					setElementStatus(elements[i].id, 'ERROR');
					continue;
				}
				 
	            var zip = '/root/diego/WordpressScraping/downloads/'+search.split(" ").join('-')+'.zip'

	            var cookies = await page._client.send('Network.getAllCookies');
	            var headers = formatWget(url[0], cookies);
	            console.log('headers ' + headers);
				await download(url[0], headers, zip , function() {
				   console.log('downloaded');
				});
	            console.log('despues de descargar')



				
			  	//await download(url[0], zip)


			 	console.log('copiado remoto')
			 	const execFile = require('child_process').execFile;
			 	console.log('copiado remoto instancia')
                var args = [
                    zip,
                    process.env.WP_REMOTE_CONN
                ];
                console.log('copiado remoto argumentos' + args)
                const child = execFile('scp', args, (err, stdout, stderr) => {
                    console.log('archivo copiado')
                   
                });


                var args = [
                    '-t',
                    process.env.WP_REMOTE_CONN2,
                    '/root/diego/permission.sh'
                ];
                console.log('permisos remotos remoto argumentos' + args)
                const child2 = execFile('ssh', args, (err, stdout, stderr) => {
                    console.log(err)
                    console.log(stdout.toString());
                    console.log('archivo copiado')
                   
                });

                setElementStatus(elements[i].id, 'DOWNLOADED', zip)

		  }catch (error){
				console.log("no encontro producto")
		  		console.log(error);
		  		//setElementStatus(elements[i].id, 'ERROR')
		  }

		}
		console.log('siguiente ' + i);
	}
  
  console.log('tomando foto')
  await page.screenshot({path: 'example.png'});
  console.log('cerrando navegador')
  await browser.close();
  console.log('fin')
})();