<?php
$host = '127.0.0.1';
$db   = 'dbpat';
$user = 'root';
$pass = 'Qw87T$djgi879';
$charset = 'utf8mb4';

$dsn = "mysql:host=$host;dbname=$db;charset=$charset";
$options = [
    PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
    PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
    PDO::ATTR_EMULATE_PREPARES   => false,
];
try {
     $pdo = new PDO($dsn, $user, $pass, $options);

     if(isset($_GET['action']) && $_GET['action'] == "error"){
     	$stmt = $pdo->query("SELECT * FROM element WHERE status = 'ERROR' order by last_updated_date desc, id asc");
     }else{
     	$stmt = $pdo->query("SELECT DISTINCT name, type, status, version,  STR_TO_DATE( last_updated_date , '%m/%d/%Y' )  as last_updated_date FROM element WHERE status = 'UPLOADED' order by STR_TO_DATE(last_updated_date, '%m/%d/%Y') desc, name asc");	
     }     	

     $date = "";
     $result = [];
     $elements = [];
     while ($row = $stmt->fetch()){
     	if($date != $row['last_updated_date']){
     		if($date != ''){
     			$result[] = array('date' => $date, 'elements' => $elements);
     			$elements = [];
     		}
     		$date = $row['last_updated_date'];
     	}
     	array_push($elements, $row);
     }

     if(count($elements) > 0){
     	$result[] = array('date' => $date, 'elements' => $elements);
     }

     echo json_encode($result);
     
		?>




<?php
		
} catch (\PDOException $e) {
     throw new \PDOException($e->getMessage(), (int)$e->getCode());
}
?>