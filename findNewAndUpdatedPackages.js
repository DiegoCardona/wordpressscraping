
var mysql = require('mysql');
const puppeteer = require('puppeteer');
require('dotenv').config()

// var save_elements = function(){

// }


function saveElement(name,type, status, version = "", last_updated_date = "", license = ""){
	var con = mysql.createConnection({
	  host: process.env.DB_HOST,
	  user: process.env.DB_USER,
	  password: process.env.DB_PASS,
	  database: process.env.DB_NAME
	});

	con.connect(function(err) {
	  if (err) throw err;
	  var sql = "INSERT INTO element (name, type ,status, version, last_updated_date) VALUES ("+mysql.escape(name)+", "+mysql.escape(type)+", "+mysql.escape(status)+", "+mysql.escape(version)+", "+mysql.escape(last_updated_date)+")";
	  con.query(sql, function (err, result) {
	    if (err) throw err;
	    // console.log("1 record inserted");
	    con.destroy();
	  });
	});
}

function processData(data){
	//console.log(JSON.stringify(data));
	data = data  + '';
	data = data.split("\n");

	var today  = new Date();
	today = today.toLocaleDateString("en-US");

	for (var i = 0; i < data.length; i++) {
		data[i] = data[i].replace(/\t/g, '')
		data[i] = data[i].split(" ");
		var version = data[i][data[i].length -1];
		data[i][data[i].length -1] = "";
		data[i] = data[i].join(' ').trim();
		if(data[i].indexOf("UPDATE") == 0){
			data[i] = data[i].replace('UPDATE','')
			saveElement(data[i], "UPDATED", "NEW", version, today)
		}
		if(data[i].startsWith("NEW")){
			data[i] = data[i].replace('NEW','')
			saveElement(data[i], "NEW", "NEW", version)
		}
	}
}

(async () => {
  const browser = await puppeteer.launch({headless: true, args: ['--no-sandbox']});
  const page = await browser.newPage();
  await page.goto(process.env.CHANGE_LOG_URL);
  await page.type('#username', process.env.CHANGE_LOG_USER);
  await page.type('#password', process.env.CHANGE_LOG_PASS);
  await page.waitForSelector('button[name="login"]');
  await page.click('button[name="login"]');
  await page.waitForNavigation();
  await page.goto(process.env.CHANGE_LOG_URL_LIST);

  const data = await page.evaluate(() => {
    const tds = Array.from(document.querySelectorAll('#content > div > div > div > table:nth-child(2) > tbody'))
    return tds.map(td => td.innerText)
  });
  console.log('a guardar data')
  processData(data)
  await page.screenshot({path: 'example.png'});
  await browser.close();
})();